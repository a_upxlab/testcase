<?php

error_reporting(-1);
ini_set('display_errors', 'On');

require_once '../Controllers/UserController.php';

(new UserController())->deleteUser();
header('Location: ' . $_SERVER['HTTP_REFERER']);
