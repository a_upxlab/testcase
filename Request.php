<?php


Class Request
{
    public static function post()
    {
        $array = array();
        foreach ($_POST as $key => $value) {
            $secureValue = htmlspecialchars($value);
            $array[$key] = $secureValue;
        }
        return $array;
    }
}
