<?php

error_reporting(-1);
ini_set('display_errors', 'On');
require_once '../vendor/autoload.php';

abstract Class Controller
{

    public function render($template, $args)
    {
        $loader = new \Twig\Loader\FilesystemLoader('../Views');
        $twig = new \Twig\Environment($loader, [
            //'cache' => 'cache',
        ]);
        echo $twig->render("$template.twig.html", $args);
    }

}
