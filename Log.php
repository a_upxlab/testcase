<?php

require_once '../vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

Class Log
{
    public static function __callStatic($name, $arguments)
    {
        $log = new Logger('testcase');
        $level = strtoupper($name);
        $log->pushHandler(new StreamHandler('../logs/info.log', constant("Monolog\Logger::".$level)));
        $log->$name($arguments[0]);
    }


}
