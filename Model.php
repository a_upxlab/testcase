<?php

require_once "Database.php";

abstract Class Model
{

    private static $fields = [];
    private $values;
    private static $table;

    public function __set($name, $value)
    {
        if (in_array($name, static::$fields)) {
            $this->values[$name] = $value;
        }
    }

    public function __get($name)
    {
        if (in_array($name, static::$fields)) {
            return $this->values[$name];
        }
    }

    public function __isset($varName)
    {
        return (bool)isset($this->values[$varName]);
    }

    /**
     *
     */
    public function save()
    {
        $db = new Database();
        $keys = implode(", ", array_keys($this->values));

        $vals = "";

        $updateValues = "";
        $types="";
        $i = 1;
        foreach ($this->values as $key => $value) {
            $updateValues .= $key . "=?";
            $types .= "ss";
            $vals .= "?";
            if ($i != sizeof($this->values)) {
                $updateValues .= ", ";
                $vals .= ", ";
            }
            $i++;
        }
        $query = "INSERT INTO " . static::$table . " ($keys) VALUES ($vals) ON DUPLICATE KEY UPDATE $updateValues";
        var_dump($query);
        $db->query($query, $types, array_merge(array_values($this->values), array_values($this->values)));
    }

    public static function find($where = null)
    {

        $db = new Database();
        $types='';
        $whereClause = "";
        if ($where) {
            $whereClause = " WHERE ";
            $i = 1;
            foreach ($where as $key => $value) {
                $whereClause .= $key . "=?";
                $types.="s";
                if ($i != sizeof($where)) {
                    $whereClause .= " AND ";
                }
                $i++;
            }

        }

        $sql = "SELECT * FROM " . static::$table . " $whereClause";

        $result = $db->query($sql, $types, $where?array_values($where):[]);


        if ($result->num_rows > 0) {
            $arrayOfModels = [];
            while ($row = $result->fetch_assoc()) {
                $model = new static();
                foreach (static::$fields as $field) {
                    $model->$field = $row[$field];
                }
                $arrayOfModels[] = $model;
            }
            return $arrayOfModels;
        } else {
        }

    }

    public function delete()
    {
        $db = new Database();
        $query = "DELETE FROM " . static::$table . " WHERE id=?";
        $db->query($query, 'i', [$this->id]);
    }
}
