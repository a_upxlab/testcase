<?php


require_once("config.php");

Class Database
{
    public $db;

    public function __construct()
    {
        $this->db = new mysqli(Config::$db_host, Config::$db_user, Config::$db_password, Config::$db_name);
        if ($this->db->connect_errno) {
            die("Cant connect: %s\n" . $this->db->connect_error);
        }

    }

    public function query($sql, $types, $a_bind_params)
    {
        $stmt = $this->db->prepare($sql);
        if ($a_bind_params) {

            $a_params = array();

            $a_params[] = &$types;

            for ($i = 0; $i < count($a_bind_params); $i++) {
                $a_params[] = &$a_bind_params[$i];
            }

            if ($stmt === false) {
                trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $this->db->errno . ' ' . $this->db->error,
                    E_USER_ERROR);
            }

            call_user_func_array(array($stmt, 'bind_param'), $a_params);
        }

        $stmt->execute();
        if ($this->db->errno) {
            throw new Exception($this->db->error);
        }
        return $stmt->get_result();
    }

}



