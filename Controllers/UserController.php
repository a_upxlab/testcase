<?php

require_once '../Controller.php';
require_once '../Request.php';
require_once '../Models/User.php';
require_once '../Models/Country.php';
require_once '../Log.php';

class UserController extends Controller
{

    public function listUsers()
    {
        $this->render('index', ['users' => User::find(), 'countries' => Country::find()]);
    }

    public function addUser()
    {
        try {
            $request = Request::post();
            $newUser = new User();
            $newUser->name = $request['name'];
            $newUser->email = $request['email'];
            $newUser->country_id = $request['country_id'];
            $newUser->save();
            Log::info('Added user with email ' . $newUser->email);
        } catch (Exception $e) {
            Log::warning($e->getMessage());
        }

    }

    public function modifyUser()
    {
        try {
            $request = Request::post();
            $user = User::find(['id' => $request['id']])[0];
            $user->name = $request['name'];
            $user->email = $request['email'];
            $user->country_id = $request['country_id'];
            $user->save();
        } catch (Exception $e) {
            Log::warning($e->getMessage());
        }

        Log::info('Modified id ' . $user->id);
    }

    public function deleteUser()
    {
        try {
            $request = Request::post();
            $user = User::find(['id' => $request['id']])[0];
            $user->delete();
        } catch (Exception $e) {
            Log::warning($e->getMessage());
        }

        Log::info('Deleted user with id ' . $user->id);
    }

}
