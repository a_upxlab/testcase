<?php

require_once '../Model.php';
require_once 'Country.php';

Class User extends Model{
    static $fields = ["id", "name", "email", "country_id"];
    static $table = "users";


    public function country()
    {
        //return
        return Country::find(['id' => $this->country_id])[0];
    }
}
