<?php

require_once '../Model.php';
Class Country extends Model{
    static $fields = ["id", "country"];
    static $table = "countries";
}
