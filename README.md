**Installation**
1. Checkout to webserver dir
2. Set web root to public folder
3. **Chmod** logs folder to write
4. Fill **config.php** to your mysql server
5. Import db.sql to your mysql db
6. Dont forget to **composer install**
